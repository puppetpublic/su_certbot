class su_certbot (
) {

  $os_family = $facts['os']['family']

  case $os_family {
    'Debian': {
      package { 'certbot': ensure => 'present' }
    }
    default: {
      crit("unknown os family '$os_family'")
    }
  }


}
