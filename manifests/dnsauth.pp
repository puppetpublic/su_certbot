# Simple wrapper for the most common DNS-authenticated certbot
# configuration for a server.

define su_certbot::dnsauth (
  Enum['present', 'absent'] $ensure = 'present',
  #
  String            $description       = '<insert one-line description here>',
  String            $subject_name      = '`hostname --fqdn`',
  Array[String]     $subject_alt_names = [],
  Array[String]     $emails            = [],
  #
  Boolean           $staging           = false,
  Boolean           $web_config_reload = true,
  Optional[String]  $keytab            = undef,
) {

  $os_family = $facts['os']['family']

  case $os_family {
    'Debian': {
      package { 'stanford-certbot-dnsauth': ensure => $ensure }
    }
    default: {
      crit("unknown os family '${os_family}'")
    }
  }

  su_certbot::config_file { $name:
    ensure               => $ensure,
    description          => $description,
    preferred_challenges => ['dns'],
    emails               => $emails,
    manual               => true,
    manual_auth_hook     => '/usr/sbin/certbot-authhook-sunetdb',
    staging              => $staging,
  }

  su_certbot::cronjob { $name:
    ensure            => $ensure,
    description       => $description,
    challenge_type    => 'dns',
    config_name       => $name,
    subject_name      => $subject_name,
    subject_alt_names => $subject_alt_names,
    logger_debug      => true,
    logger_syslog     => true,
    web_config_reload => $web_config_reload,
    require           => Su_certbot::Config_file[$name],
    keytab            => $keytab,
  }

}
