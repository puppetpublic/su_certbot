# Simple wrapper for the most common HTTP-authenticated certbot
# configuration for a server.

define su_certbot::httpauth (
  Enum['present', 'absent'] $ensure = 'present',
  #
  String         $description       = '<insert one-line description here>',
  String         $subject_name      = '`hostname --fqdn`',
  Array[String]  $subject_alt_names = [],
  Array[String]  $emails            = [],
  Enum['apache'] $web_server        = 'apache',
  #
  Boolean        $staging           = false,
  Boolean        $web_config_reload = true,
) {

  $os_family = $facts['os']['family']

  # Install packages.
  case $os_family {
    'Debian': {
      case $web_server {
        'apache': {
          ensure_packages(['python3-certbot-apache'], {'ensure' => 'present'})
        }
        'nginx': {
          ensure_packages(['python3-certbot-nginx'], {'ensure' => 'present'})
        }
        default: {
          fail("unknown os family '${os_family}'")
        }
      }
    }
    default: {
      fail("unknown os family '${os_family}'")
    }
  }

  # Install the certbot configuration file. Since we
  # are using a web server plugin we set manual to false and
  # use the plugin instead in the cronjob wrapper script.
  su_certbot::config_file { $name:
    ensure               => $ensure,
    description          => $description,
    preferred_challenges => ['http'],
    emails               => $emails,
    manual               => false,
    staging              => $staging,
  }

  # Install the certbot script that the cronjob will run as well as the
  # cronjob file itself.
  su_certbot::cronjob { $name:
    ensure            => $ensure,
    description       => $description,
    challenge_type    => 'http',
    plugin            => $web_server,
    config_name       => $name,
    subject_name      => $subject_name,
    subject_alt_names => $subject_alt_names,
    web_config_reload => $web_config_reload,
    require           => Su_certbot::Config_file[$name],
  }

}
