define su_certbot::cronjob (
  Enum['present', 'absent'] $ensure = 'present',
  #
  String $description = "Basic server certificate provisioning.",
  #
  Enum['dns', 'http'] $challenge_type = undef,
  Optional[Enum['apache', 'nginx']]
                      $plugin         = undef,
  #
  String           $subject_name      = '`hostname --fqdn`',
  Array[String]    $subject_alt_names = [],
  #
  String           $config_basedir = '/etc/letsencrypt',
  String           $config_name    = undef,
  #
  # Apache reload setting.
  Boolean          $web_config_reload = true,
  #
  # cron settings
  Enum['cron.d', 'hourly', 'daily', 'weekly', 'monthly']
                   $cron_type      = 'daily',
  String           $crond_schedule = '30 2 * * *',
  #
  # certbot-authhook-sunetdb settings (ignored if ???)
  Boolean          $logger_debug   = false,
  Boolean          $logger_syslog  = false,
  Boolean          $logger_stdout  = false,
  Optional[String] $logger_file    = undef,
  Optional[String] $keytab         = undef,
  Optional[String] $dns_wait_secs  = undef,
) {

  # Validation

  # If the challenge type is "http" make sure that the plugin parameter
  # is defined.
  if (($challenge_type == 'http') and (! $plugin)) {
        crit("missing required plugin for http challenge type")
  }

  # If the cron job that comes with the certbot package is still in
  # /etc/cron.d move it to /etc/letsencrypt and rename to certbot.crond.orig
  if ($ensure == 'present') {
   include su_certbot::remove_original_certbot_cron
  }

  ## The script the cronjob calls.
  $cronjob_wrapper_name = "/etc/letsencrypt/cronjob-wrapper-${name}"

  ## The full path to config file
  $config_path = "${config_basedir}/${config_name}.ini"

  case $challenge_type {
    'dns':   { $cronjob_erb_file = 'cronjob-wrapper-dns.erb'    }
    'http':  { $cronjob_erb_file = 'cronjob-wrapper-http.erb'   }
    default: { crit("unknown challenge type '${challenge_type}'") }
  }

  file { $cronjob_wrapper_name:
    ensure  => $ensure,
    content => template("su_certbot/etc/letsencrypt/${cronjob_erb_file}"),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }


  ## The cronjob itself.
  $cron_type_to_dir = {
    'cron.d'  => '/etc/cron.d',
    'hourly'  => '/etc/cron.hourly',
    'daily'   => '/etc/cron.daily',
    'weekly'  => '/etc/cron.weekly',
    'monthly' => '/etc/cron.monthly',
  }

  $cron_dir = $cron_type_to_dir[$cron_type]

  if ($cron_type == 'cron.d') {
    $cron_file_mode = '0644'
  } else {
    $cron_file_mode = '0755'
  }

  file { "$cron_dir/certbot-${name}":
    ensure  => $ensure,
    content => template('su_certbot/etc/cron/cronjob.erb'),
    mode    => $cron_file_mode,
    owner   => 'root',
    group   => 'root',
  }

}
