# Remove the original file /etc/cert.d/certbot that gets installed with the
# Debian "certbot" package. We remove it since we are installing our
# own certbot cron jobs.

class su_certbot::remove_original_certbot_cron {

  # If the cron job that comes with the certbot package is still in
  # /etc/cron.d move it to /etc/letsencrypt and rename to certbot.crond.orig
  exec { "remove_crond_certbot":
    path    => ['/usr/bin', '/usr/sbin'],
    onlyif  => 'test -f /etc/cron.d/certbot',
    command => ['/usr/bin/mv', '/etc/cron.d/certbot', '/etc/letsencrypt/certbot.crond.orig'],
  }

}
