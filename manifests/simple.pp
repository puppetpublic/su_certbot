# Use this define for basic http or DNS challenge certbot configurations
# that do not need any special configurations.

define su_certbot::simple (
  Enum['present', 'absent'] $ensure = 'present',
  #
  String              $description       = '<insert one-line description here>',
  Enum['dns', 'http'] $challenge_type    = undef,
  #
  String              $subject_name      = '`hostname --fqdn`',
  Array[String]       $subject_alt_names = [],
  Array[String]       $emails            = [],
  #
  Optional[Enum['apache', 'nginx']]
                      $web_server        = 'apache',
  #
  Boolean $staging = false,
) {

  include su_certbot

  case $challenge_type {
    'dns': {
      su_certbot::dnsauth { $name:
        ensure            => $ensure,
        description       => $description,
        subject_name      => $subject_name,
        subject_alt_names => $subject_alt_names,
        emails            => $emails,
        staging           => $staging,
      }
    }
    'http': {
      su_certbot::httpauth { $name:
        ensure            => $ensure,
        description       => $description,
        subject_name      => $subject_name,
        subject_alt_names => $subject_alt_names,
        emails            => $emails,
        web_server        => $web_server,
        staging           => $staging,
      }
    }
    default: {
      crit("unknown challenge type '${challenge_type}'")
    }
  }

}
