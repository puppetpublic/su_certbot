define su_certbot::config_file (
  Enum['present', 'absent'] $ensure = 'present',
  #
  String        $config_basedir       = '/etc/letsencrypt',
  #
  String        $description          = "<insert one-line description here>",
  Array[String] $emails               = [],
  Array[String] $preferred_challenges = ['https'],
  #
  Boolean          $manual               = true,
  Optional[String] $manual_auth_hook     = undef,
  Optional[String] $manual_cleanup_hook  = undef,
  #
  Boolean       $staging              = false,
  Boolean       $dry_run              = false,
  Boolean       $agree_tos            = true,
  Boolean       $non_interactive      = true,
  #
  # Any extra settings:
  Array[String] $extra_settings = [],
)
{

  file { "$config_basedir/${name}.ini":
    ensure  => $ensure,
    content => template('su_certbot/etc/letsencrypt/config.ini.erb'),
  }

}
