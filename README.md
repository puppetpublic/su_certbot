[[_TOC_]]

# The `su_certbot` Puppet module

## Quick Links

* ["Add ACME challenge records to the stanford.edu DNS zone"][10] (from the Networking Group)

* [`certbot-authhook-sunetdb` man page][13]

* [Certbot User Guide][12]

## Overview

A few definitions:

* **ACME**: an [IETF Proposed standard][4] for automated
certificate issuance. ACME stands for "Automatic Certificate
Management Environment".

* **[Let's Encrypt][7]**: a non-profit Certificate Authority providing
free 90-day domain-validated certificates via ACME.

* **[Certbot][5]**: a free tool for provisioning Let's Encrypt certificates on
websites; Certbot is maintained by the [EFF][6] (Electronic Frontier
Foundation).

This `su_certbot` Puppet module creates a Certbot configuration file and
cron job to manage Let's Encrypt certificates. It supports both [HTTP and
DNS ACME challenges][1].

## The `su_certbot` defined resources

In most cases you should be able to use the `su_certbot::simple` Puppet
[defined resource][11]. If you need more control over the Certbot
parameters you can instead use the two defined resources
`su_certbot::cronjob` and `su_certbot::config_file`.

### The `su_certbot::simple` defined resource

In most cases you should be able to use the `su_certbot::simple` Puppet
[defined resource][11]. The Puppet `su_certbot::simple` defined resource
creates three objects:

* a Certbot configuration file in `/etc/letsencrypt/`

* a bash script that runs the Certbot command to provision the Let's
Encrypt certificates; this script points at the Certbot configuration file
above

* a daily cron job that runs the above Certbot script

#### Parameters

* `ensure`: one of `absent` or `present`. Default: `present`.

* `description`: a one-line description used as a comment. Example:
"manage Let's Encrypt certificates for the Debian Repository server".

* `challenge_type`: one of `http` or `dns`. Default: None.

* `subject_name`: The fully-qualified domain name that goes in the Subject
of the certificate. Example: `debian-repo-dev.stanford.edu`.
Default: `hostname --fqdn`.

* `subject_alt_names`: An array of [Subject Alternative Names][2] to be
added to the certificate. Default: the empty array.

* `emails`: An array of e-mail addresses that Let's Encrypt will use to
send [expiration warning notices][3]; note that when used as recommended
Certbot will renew your certificate before you ever need to get such a
warning. Default: the empty array.

* `web_server`: if using the `http` challenge method this tells Certbot
which web server you are running so it can do the necessary temporary web
server configuration for certificate privisioning. Currently this
parameter can only be one of `apache` or `nginx`. Use this parameter only
when using the `http` challenge method.

* `staging`: set to `true` if you want Certbot to provision a test Let's
Encrypt certificate. These test certificates' root certificates will not
be honored by web browsers. See also section ["Running in staging
mode"](#running-in-staging-mode) below.

#### Examples

##### Example 1: Public web service, Apache, and a single certificate name

In this case we assume that there is an Apache web server running
that accepts connections on port 80 from the public internet.
Because this service accepts port 80 connections from everywhere
we can use the HTTP challenge. Note that we do not specify the
`subject_name` parameter as it defaults to `hostname --fqdn`.
```
su_certbot::simple { 'my-public-server':
  ensure         => present,
  description    => "manage Let's Encrypt certificates",
  challenge_type => 'http',
  web_server     => 'apache',
}
```

##### Example 2: public web service, Apache, and multiple certificate names

This is the same as the preceding case except we have two names we want to
use as Subject Alternative Names: `myserver1.stanford.edu` and
`myserver2.stanford.edu`.

```
su_certbot::simple { 'my-public-server':
  ensure            => present,
  description       => "manage Let's Encrypt certificates with SANs",
  challenge_type    => 'http',
  web_server        => 'apache',
  subject_alt_names => [
    'myserver1.stanford.edu',
    'myserver2.stanford.edu',
  ],
}
```

##### Example 3: service with private web service

In this case the public internet cannot reach our web server so we need to
use the DNS challenge method. Note that extra set-up around Kerberos is
required for this to work; see section ["The ACME DNS challenge at
Stanford"](#the-acme-dns-challenge-at-stanford) for more information.
```
su_certbot::simple { 'my-public-server':
  ensure            => present,
  description       => "manage Let's Encrypt certificates with SANs (DNS challenge)",
  challenge_type    => 'dns',
  subject_alt_names => [
    'myserver1.stanford.edu',
    'myserver2.stanford.edu',
  ],
}
```

### The `su_certbot::config_file` defined resource

The `su_certbot::config_file` defined resource creates a file that can be
used with Certbot's `--config` parameter; see the [Certbot User Guide][12]
for information on this configuration file.

#### Parameters

* `ensure`: one of `absent` or `present`. Default: `present`.

* `description`: a one-line description used as a comment. Example:
"manage Let's Encrypt certificates for the Debian Repository server".

* `config_basedir`: where to store the Let's Encrypt certificates.
Default: `/etc/letsencrypt`.

The following parameters directly correspond to the options described
in the [Certbot User Guide][12]:
```
preferred_challenges (default: ['http'])
emails               (default: [])
staging              (default: false)
manual               (default: true)
manual_auth_hook     (default: undef)
manual_cleanup_hook  (default: undef)
dry_run              (default: false)
agree_tos            (default: true)
non_interactive      (default: true)
```

### The `su_certbot::cronjob` defined resource

#### Parameters

The following parameters are exactly the same as for
`su_certbot::simple`:
```
ensure
description
challenge_type
subject_name
subject_alt_names
```

Other parameters:

* `plugin`: the webserver plugin that Certbot should use when doing an HTTP challenge.
Currently the only accepted values are `apache` and `nginx`.

* `config_basedir`: where to store the Let's Encrypt certificates.
Default: `/etc/letsencrypt`.

* `config_name`: the name (without extension) of the Certbot configuration file
that the cron job should use. This configuration file should be in the
`config_basedir` directory. Default: `undef`.

* `cron_type`: what sort of cron job. Must be one of
`cron.d`, `hourly`, `daily`, `weekly`, `monthly`. Default: `daily`.

* `crond_schedule`: if `cron_type` is set to `cron.d`, then
`crond_schedule` should be set to a cron-compatible schedule.
Default: `30 2 * * *`.

The following settings are used only when doing a DNS challenge with
`certbot-authhook-sunetdb`; see the [man page for
`certbot-authhook-sunetdb`][13] for more details on what these settings
do.

```
logger_debug    (Default: false)
logger_syslog   (Default: false)
logger_stdout   (Default: false)
logger_file     (Default: undef)
keytab          (Default: undef)
dns_wait_secs   (Default: undef)
```

## The ACME DNS challenge at Stanford

To perform an ACME DNS challenge Certbot needs to run a hook-script
that does the DNS registration. The `su_certbot` module uses the
[certbot-authhook-sunetdb][9] script for this hook. The
certbot-authhook-sunetdb script is based on the instructions at the
["Add ACME challenge records to the stanford.edu DNS zone"][10] page. In
order for this script to work you must have a Kerberos keytab file with a
`host/` principal for every name that you want in the certificate.
`su_cerbot` looks at `/etc/krb5.keytab` by default.

The `su_certbot` module does _not_ manage the Kerberos keytab; you must do
that yourself.

Example: use a DNS challenge to register a certificate with the names
`nagios.stanford.edu`, `nagios01.stanford.edu`, and
`nagios-prod.stanford.edu`. Credentials for the three Kerberos principals
`host/nagios.stanford.edu`, and `host/nagios01.stanford.edu`, and
`host/nagios-prod.stanford.edu` must be in `/etc/krb5.keytab`.


## Chicken-and-egg and the HTTP challenge method

If you are using the HTTP challenge method with Apache you may run into a
"chicken-and-egg" problem when provisioning the Let's Encrypt certifates
the first time. The way that Certbot gets its certificates when running on
an Apache server is to create a special Apache configuration file and then
reload the Apache configuration. But if part of your Apache configuration
is pointing at Let's Encrypt certificates that _do not yet exist_ (because
this is the first time), the Apache service will not start.

To get around this you can disable that part of the Apache configuration
that points to the not-yet-in-existence Let's Encrypt certificate, reload
Apache, and then run the Certbot script by hand.

Another way around this is to create a self-signed key-pair that the
Apache configuration points to while it is waiting for the real Let's
Encrypt key-pair. You can do this with the `su_apache::cert::lets_encrypt`
defined resource from the [`su_apache` Puppet module][14].


## Running in staging mode

Use the "staging" mode option with certbot during testing. If you don't
you can run into [Let's Encrypt's rate limits][8] (at the time of this
writing only five requests per certificate-name per week). Running in
staging mode means that the certificates you get from Let's Encrypt are
rooted in a certificate that is not recognized by the web browser.

Here is an example of running in "staging" mode:
```
su_certbot::simple { 'my-public-server':
  ensure         => present,
  staging        => true,
  description    => "manage Let's Encrypt certificates",
  challenge_type => 'http',
  web_server     => 'apache',
}
```




[1]: https://letsencrypt.org/docs/challenge-types/

[2]: https://en.wikipedia.org/wiki/Subject_Alternative_Name

[3]: https://letsencrypt.org/docs/expiration-emails/

[4]: https://datatracker.ietf.org/doc/html/rfc8555

[5]: https://certbot.eff.org/

[6]: https://www.eff.org/

[7]: https://letsencrypt.org/

[8]: https://letsencrypt.org/docs/rate-limits/

[9]: https://code.stanford.edu/orange/stanford-certbot-dnsauth

[10]: https://web.stanford.edu/group/networking/acme-dns/

[11]: https://puppet.com/docs/puppet/6/lang_defined_types.html

[12]: https://eff-certbot.readthedocs.io/en/stable/using.html

[13]: https://code.stanford.edu/orange/stanford-certbot-dnsauth/-/tree/master/manpages/certbot-authhook-sunetdb

[14]: https://code.stanford.edu/suitpuppet/su_apache
